__all__ = ['DummyChannel']

from Channel.SMChannel import Store


class DummyChannel(Store):
    def __init__(self, name= None):
        super().__init__(capacity= 0)

    def ready(self, amode):
        return False


