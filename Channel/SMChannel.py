"""Shared memory channel
   implemented using an in-memory FIFO buffer
"""

import threading

from common import *
from .Access import *

logger= logging.getLogger(__name__)


@dataclass()
class Gauge:
    _capacity : int

    @property
    def capacity(self):
        return self._capacity

    @property
    def size(self):
        raise NotImplementedError

    def __init__(self, capacity= None, **kwargs):
        super().__init__(**kwargs)
        self._capacity= capacity

    def nonempty(self):
        return 0 != self.size

    def nonfull(self):
        return self.capacity != self.size


class StoreAccess(Access):
    @dataclass
    class Accessor(Access.Accessor):
        ref: object #Store

        def __init__(self, ref):
            super().__init__()
            self.ref= ref

        def attached(self):
            pass

        def detached(self):
            pass

    class Source(Accessor, Access.Source):
        def ready(self):
            return self.ref.nonempty()

        def get(self):
            return self.ref.get()

    class Target(Accessor, Access.Target):
        def ready(self):
            return self.ref.nonfull()

        def put(self, data):
            self.ref.put(data)


class Store(Gauge, StoreAccess):
    def __init__(self, capacity= None):
        super().__init__(capacity= capacity, ref= self)

    def get(self):
        raise NotImplementedError

    def put(self, data, /):
        raise NotImplementedError


@dataclass
class Buffer(Store):
    _name   :   str
    _buf    :   list
    _lock   :   threading.Lock

    def __init__(self, capacity= None, name= None):
        super().__init__(capacity= capacity)
        self._name= name
        self._buf= []
        self._lock= threading.Lock()

    @property
    def size(self):
        with self._lock:
            return len(self._buf)

    def get(self):
        with self._lock:
            data= self._buf.pop(0)
            # logger.debug('{}: {} <- {}'.format(self._name, data, self._buf))
            self.accessor(Access.Target).signal()
            return data

    def put(self, data, /):
        with self._lock:
            # logger.debug('{}: {} <- {}'.format(self._name, self._buf, data))
            self._buf.append(data)
            self.accessor(Access.Source).signal()


class SMBufferChannel(Buffer):
    pass
