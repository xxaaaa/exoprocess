__all__ = ['SMCD', 'DMCD']

from common import *

import Channel.SMChannel as SMChannel
import Channel.DMChannel as DMChannel

logger= logging.getLogger(__name__)


@dataclass()
class CD:
    """Channel Descriptor
    """

    _name       : str
    _capacity   : int

    def __init__(self, name, capacity= None):
        self._name= name
        self._capacity= capacity

    def register(self):
        raise NotImplementedError

    class ExoCD:
        def __init__(self, **kwargs):
            self.__dict__ = kwargs

        def get_accessor(self, amode):
            raise NotImplementedError

    def exo_descriptor(self, **kwargs):
        #logger.debug('=====')
        return self.ExoCD(**kwargs)


@dataclass()
class SMCD(CD):
    """Shared Memory Channel Descriptor
    """
    _channel: SMChannel.SMBufferChannel

    def __init__(self, name, capacity= None):
        super().__init__(name= name, capacity= capacity)
        self._channel= None

    def register(self):
        self._channel= SMChannel.SMBufferChannel(capacity= self._capacity, name= self._name)

    class ExoCD(CD.ExoCD):
        def get_accessor(self, amode):
            return self.channel.accessor(amode)

    def exo_descriptor(self):
        return super().exo_descriptor(channel= self._channel)


@dataclass()
class DMCD(CD):
    """Distributed Memory Channel Descriptor
    """
    _endochannel: DMChannel.DMEndoChannel

    def __init__(self, name, capacity= None):
        super().__init__(name= name, capacity= capacity)

    def register(self):
        self._endochannel= DMChannel.DMEndoChannel(capacity= self._capacity, name= self._name)

    class ExoCD(CD.ExoCD):
        def get_accessor(self, amode):
            return DMChannel.ConnAccess(conn= self.conns[amode]).accessor(amode)

    def exo_descriptor(self):
        return super().exo_descriptor(conns= self._endochannel.connections())
