import unittest
from Channel.SMChannel import *


# class VariableTest(unittest.TestCase):
#     def test(self):
#         v= Variable()
#         s= v.accessor(Access.Source)
#         t= v.accessor(Access.Target)
#
#         self.assertIsNone(v.value)
#
#         v.value= 1
#         self.assertEqual(1, v.value)
#         self.assertEqual(1, s.get())
#
#         t.put(2)
#         self.assertEqual(2, v.value)
#         self.assertEqual(2, s.get())


class BufferTest(unittest.TestCase):
    def test(self):
        b= Buffer()

        self.assertIsNone(b.capacity)
        s= b.accessor(Access.Source)
        t= b.accessor(Access.Target)
        self.assertFalse(s.ready())
        self.assertTrue(t.ready())

        t.put(1)
        self.assertTrue(s.ready())
        x= s.get()
        self.assertEqual(1, x)


