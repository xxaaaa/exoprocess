import unittest
from Channel.Access import *


class AccessTest(unittest.TestCase):
    def test(self):
        a= Access()

        self.assertIsNone(a._accessors.get(Access.Source))
        self.assertIsNone(a._accessors.get(Access.Target))
        s= a.accessor(Access.Source)
        self.assertIsNotNone(a._accessors.get(Access.Source))
        t= a.accessor(Access.Target)
        self.assertIsNotNone(a._accessors.get(Access.Target))
        self.assertTrue(isinstance(s, Access.Source))
        self.assertTrue(isinstance(t, Access.Target))
        self.assertIs(s.amode(), Access.Source)
        self.assertIs(t.amode(), Access.Target)

        self.assertIsNone(s._linked)
        self.assertIsNone(t._linked)


if __name__ == '__main__':
    unittest.main()
