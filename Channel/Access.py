__all__ = ['Access', 'REvent']

import threading
from common import *

logger= logging.getLogger(__name__)


class REvent:
    """Recurrent event
    """

    def __init__(self):
        super().__init__()
        self._cond= threading.Condition(lock= threading.Lock())
        self._flag= False

    def signal(self):
        with self._cond:
            self._flag= True
            self._cond.notify()

    def wait(self):
        with self._cond:
            self._cond.wait_for(predicate= lambda: self._flag)
            self._flag= False


@dataclass
class Access:
    _kwargs   : dict
    _accessors: dict

    @dataclass
    class Accessor:
        _linked: object #Accessor
        _event: REvent
        _elock: threading.Lock

        def __init__(self):
            super().__init__()
            self._linked= None
            self._event= None
            self._elock= threading.Lock()

        def setevent(self, event= None, /):
            with self._elock:
                ready= self.ready()
                self._event= None if ready else event
                return ready

        def signal(self):
            with self._elock:
                if self._event:
                    self._event.signal()
                    self._event= None

        def attach(self, accessor):
            self._linked= accessor
            self._linked.attached()

        def detach(self):
            self._linked.detached()
            self._linked= None

        def attached(self):
            raise NotImplementedError

        def detached(self):
            raise NotImplementedError

        def amode(self):
            """Access Mode: Access.Source | Access.Target"""
            raise NotImplementedError

        def ready(self):
            raise NotImplementedError

        def transfer(self):
            raise NotImplementedError

    class Source(Accessor):
        def amode(self):
            return Access.Source

        def get(self):
            raise NotImplementedError

        def transfer(self):
            return self._linked.put(self.get())

    class Target(Accessor):
        def amode(self):
            return Access.Target

        def put(self, data, /):
            raise NotImplementedError

        def transfer(self):
            return self.put(data= self._linked.get())

    def __init__(self, **kwargs):
        self._kwargs= kwargs
        self._accessors= {}

    @staticmethod
    def inverse(amode, /):
        return {
            Access.Source: Access.Target,
            Access.Target: Access.Source
        }[amode]

    def _create(self, amode, /):
        if amode is Access.Source:
            return self.Source(**self._kwargs)
        if amode is Access.Target:
            return self.Target(**self._kwargs)

    def amode(self):
        raise NotImplementedError

    def accessor(self, amode= None, /):
        amode= amode if amode else self.inverse(self.amode())
        if amode not in self._accessors:
            self._accessors[amode]= self._create(amode)
        return self._accessors[amode]


