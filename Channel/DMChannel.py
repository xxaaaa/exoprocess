"""Distributed memory point-to-point channel
   implemented using multiprocessing.Pipe
"""

import threading

from common import *
from .Access import *
import multiprocessing as MP
import multiprocessing.connection as MPC
import select

logger= logging.getLogger(__name__)


@dataclass
class DMEndoChannel:
    source: MPC.Connection
    target: MPC.Connection

    def __init__(self, **kwargs):
        self.source, self.target= MP.Pipe(duplex= False)

    def connections(self):
        return {
            Access.Source: self.source,
            Access.Target: self.target
        }


# @dataclass()
# class DMEndoChannel(Buffer):
#     pass
#
#     _name: str
#     _inpS: Connection
#     _inpT: Connection
#     _outS: Connection
#     _outT: Connection
#
#     def __init__(self, capacity= None, name= None):
#         super().__init__(capacity= capacity)
#         self._name= name
#         # self._buffer= []
#         self._inpS, self._inpT = Pipe(duplex= False)
#         self._outS, self._outT = Pipe(duplex= False)
#         threading.Thread(target= self.listener, daemon= True).start()
#
#     def connections(self):
#         return {AccessMode.SOURCE: self._outS, AccessMode.TARGET: self._inpT}
#
#     def listener(self):
#         while True:
#             i= [self._inpS] if self.nonfull()  else []
#             o= [self._outT] if self.nonempty() else []
#             i, o, _ = select.select(i, o, [])
#
#             if 0 < len(i):
#                 data= self._inpS.recv()
#                 self.dop_TARGET(data= data)
#
#             if 0 < len(o):
#                 data= self.dop_SOURCE()
#                 self._outT.send(data)


class ConnAccess(Access):
    @dataclass
    class Accessor(Access.Accessor):
        _conn   : MPC.Connection
        _ready  : bool
        _rlock  : threading.Lock
        _revent : REvent

        def __init__(self, conn):
            super().__init__()
            self._conn= conn
            self._ready= False
            self._rlock= threading.Lock()
            self._revent= REvent()
            threading.Thread(target= self.updater, name= self.amode().__name__, daemon= True).start()

        def ready(self):
            #logger.debug('+{}'.format(self._ready))
            with self._rlock:
                return self._ready

        def attached(self):
            pass

        def detached(self):
            self._conn.close()

        def updater(self):
            while True:
                select.select([self._conn], [self._conn], [])
                with self._rlock:
                    self._ready= True

                self.signal()
                self._revent.wait()

    @dataclass(init= False)
    class Source(Accessor, Access.Source):
        def get(self):
            data= self._conn.recv()
            self._ready= False
            self._revent.signal()
            return data

    @dataclass(init= False)
    class Target(Accessor, Access.Target):
        def put(self, data, /):
            self._conn.send(data)
            self._ready= False
            self._revent.signal()



