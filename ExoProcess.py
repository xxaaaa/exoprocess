"""
ExoProcess
"""

__all__ = ['SMCD', 'DMCD', 'ExoProcess', 'ExoModule', 'ExoReactor', 'IFRecord', 'IFVector', 'S', 'T', 'Portal']

from common import *
from ExoPort import *

#from Channel.DummyChannel import *
from Channel.Descriptors import *

from Context.Registration import *
import Context.SMContext
import Context.ModuleContext

import Interaction


logger= logging.getLogger(__name__)

#S.outer_accessor().attach(DummyChannel())
#T.outer_accessor().attach(DummyChannel())


@dataclass()
class SubControl:
    """subprocess control interface available to ExoProcesses

    """
    _registrator  : Registrator

    @staticmethod
    def start_all(*args, **kwargs):
        for a in args:
            a.start(**kwargs)

    @staticmethod
    def join_all(*args):
        for a in args:
            a.join()

    def __init__(self, registrator):
        self._registrator= registrator if registrator else Context.SMContext.SMRegistrator

    def registerChannel(self, *cds):
        """register channels to be attached to exoprocess ports
        :param  channel descriptors
        :return the same channel descriptors, but registered
        """
        for d in cds:
            d.register()

        return cds if 1 < len(cds) else cds[0]

    @staticmethod
    def _registerProcess(exoprocess, registrator):
        return registrator.register(exoprocess= exoprocess)

    def registerProcess(self, *processes):
        """register exoprocesses
        :param  ExoProcess(es) to be registered
        :return EndoContext object(s) encapsulating the registered ExoProcess(es)
        """
        xs= []
        for p in processes:
            x= self._registerProcess(exoprocess= p, registrator= self._registrator)
            xs.append(x)

        return xs if 1 < len(xs) else xs[0]


@dataclass(repr= False)
class ExoProcess(SubControl):
    """ExoProcess class, to be subclassed by applications
    """
    _exo          : object # reference to the exocontext
    _name         : str    # name of the process for logging purposes

    # >> forwarding to the exocontext >>>>>>>>>>>>>>
    def __getattr__(self, key):
        return getattr(self._exo, key)

    def __getitem__(self, key):
        return self._exo[key]

    # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    def __init__(self, name= None, registrator= None):
        super().__init__(registrator= registrator)
        self._exo= None
        self._name= name if name else self.__class__.__name__

    def interaction(self, portal):
        """general interaction
        """
        return Interaction.interaction(portal= portal)

    # >> special interactions as special cases of general interaction >>>>>>>>>>

    def recv(self, port):
        Interaction.interaction(portal= Portal(p= port))
        return port.value

    def send(self, port, **kwargs):
        port.value= kwargs.get('value', port.value)
        Interaction.interaction(portal= Portal(p= port))
        return port.value

    def bcast(self, targets, **kwargs):
        ports= targets.ports()
        for p in ports:
            p.value= kwargs.get('value', p.value)
        Interaction.interaction(portal= Portal(*ports))

    # <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    def _body(self, **kwargs):
        logger.debug('started.')
        self.body(**kwargs)
        logger.debug('terminated.')

    def body(self, interface= None, **kwargs):
        """body of the process, to be overridden in subclasses
        :param interface: ExoInterface (port structure)
        :param kwargs: startup parameters
        :return: not used
        """
        pass


class ExoModule(ExoProcess):
    """class to promote a python module to an ExoProcess
    """
    std_interface= IFRecord(stdin= S, stdout= T, stderr= T)

    def __init__(self, registrator= None):
        super().__init__(name= type(self).__name__, registrator= registrator)

    def start(self, **kwargs):
        x= self._registerProcess(exoprocess= self, registrator= Context.ModuleContext.ModuleRegistrator())
        x.start(**kwargs)

    def body(self, interface= std_interface, **kwargs):
        raise NotImplementedError


class ExoReactor(ExoProcess):
    """ExoProcess subclass to support standard reactive behaviour
    """

    def terminate(self):
        raise NotImplementedError

    def prologue(self):
        raise NotImplementedError

    def epilogue(self):
        raise NotImplementedError

    def compute(self):
        raise NotImplementedError

    def communicate(self):
        raise NotImplementedError

    def body(self, interface= None, **kwargs):
        self.prologue()

        while not self.terminate():
            self.compute()
            if self.terminate():
                break

            self.communicate()

        self.epilogue()



