__all__ = ['Registrator']

from common import *
logger= logging.getLogger(__name__)


class Registrator:
    @staticmethod
    def register(exoprocess):
        raise NotImplementedError

