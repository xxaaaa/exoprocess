__all__ = ['get_context']

import threading
from .Base import *
from .Registration import *
from .SMContext import SMExoContext


def get_context(exoprocess):
    x= SMExoContext(exoprocess= exoprocess)
    return ModuleContext(exocontext= x)


class ModuleRegistrator(Registrator):
    @staticmethod
    def register(exoprocess):
        x = SMExoContext(exoprocess=exoprocess)
        return ModuleContext(exocontext=x)


class ModuleContext(EndoContext):
    def __getattr__(self, item):
        return getattr(self._exocontext, item)

    def start(self, **kwargs):
        self._executor= threading.current_thread()
        self._executor._exoprocess= self._exocontext._exoprocess
        self._exocontext.start(**kwargs)

    def join(self):
        pass

    def interrupt(self):
        pass
