__all__ = ['InterfaceControl', 'EndoControl', 'ExoControl']

from common import *

logger = logging.getLogger(__name__)


class InterfaceControl:
    def get_amode(self, ref=''):
        raise NotImplementedError

    def extend(self, ref= '', amount= 1):
        raise NotImplementedError

    def attach(self, ref= '', **kwargs):
        raise NotImplementedError

    def detach(self, ref= ''):
        raise NotImplementedError

    def commit(self):
        raise NotImplementedError


class EndoControl(InterfaceControl):
    """parent -> subprocess
    """

    # execution
    #
    def start(self, **kwargs):
        raise NotImplementedError

    def join(self):
        raise NotImplementedError

    def interrupt(self):
        raise NotImplementedError


class ExoControl:
    """process -> context
    """

    def refresh(self):
        raise NotImplementedError

    def ports(self):
        raise NotImplementedError

    # +access to process interface (ports)
