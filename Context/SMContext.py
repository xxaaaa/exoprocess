__all__ = ['SMRegistrator']

from common import *
import threading
from .Registration import *
from .Base import *


logger= logging.getLogger(__name__)


class SMRegistrator(Registrator):
    @staticmethod
    def register(exoprocess):
        x= SMExoContext(exoprocess= exoprocess)
        return SMEndoContext(exocontext= x)


class SMEndoContext(EndoContext):
    def start(self, **kwargs):
        self._executor= threading.Thread(target= self._exocontext.start, kwargs= kwargs)
        exoprocess= self._exocontext._exoprocess
        self._executor._exoprocess= exoprocess
        self._executor.name = exoprocess._name if exoprocess._name else exoprocess.__class__.__name__
        self._executor.start()

    def join(self):
        return self._executor.join()

    def interrupt(self):
        raise NotImplementedError

    def _forward(self, function, **kwargs):
        return getattr(self._exocontext, function)(**kwargs)


class SMExoContext(ExoContext):
    #>> parent -> subprocess

    def start(self, **kwargs):
        self.commit()
        self.refresh()

        kwargs['interface']= self.interface
        self._exoprocess.body(**kwargs)




