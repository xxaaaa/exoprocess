__all__ = ['DMRegistrator']

from common import *
import multiprocessing
import threading
import inspect
from .Registration import *
from .Base import *


logger= logging.getLogger(__name__)


class DMRegistrator(Registrator):
    @staticmethod
    def register(exoprocess):
        x= DMExoContext(exoprocess= exoprocess)
        return DMEndoContext(exocontext= x)


@dataclass()
class DMEndoContext(EndoContext):
    _queue= multiprocessing.SimpleQueue

    def __init__(self, exocontext):
        super().__init__(exocontext= exocontext)
        self._queue= multiprocessing.SimpleQueue()

    def start(self, **kwargs):
        self._executor= multiprocessing.Process(target= self._exocontext.start, args= (self._queue,), kwargs= kwargs)
        self._exocontext= None
        self.commit()
        self._executor.start()

    def join(self):
        return self._executor.join()

    def _forward(self, function, **kwargs):
        # if self._exocontext:
        #     return getattr(self._exocontext, function)(**kwargs)
        # else:
        self._queue.put((function, kwargs))


@dataclass()
class DMExoContext(ExoContext):

    def updater(self, queue, condition= lambda:False):
        while not condition():
            f, kwargs= queue.get()
            #logger.debug((f, kwargs))
            getattr(self, f)(**kwargs)

    def __init__(self, exoprocess):
        super().__init__(exoprocess= exoprocess)

    #>> parent -> subprocess

    def start(self, queue, **kwargs):
        self.updater(queue= queue, condition= queue.empty)
        self.refresh()
        threading.Thread(target= self.updater, name= 'updater', args= (queue,), daemon= True).start()

        threading.current_thread().name= self._exoprocess._name if self._exoprocess._name else self._exoprocess.__class__.__name__
        kwargs['interface']= self.interface
        self._exoprocess.body(**kwargs)
