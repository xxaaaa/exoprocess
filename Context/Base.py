__all__ = ['EndoContext', 'ExoContext']

from common import *
from .ExoControl import *
from ExoPort import ExoInterface
from rodict import rodict
import inspect
import threading

logger= logging.getLogger(__name__)


@dataclass
class ContextInterface(InterfaceControl):
    _lock       : threading.Lock
    _committed  : ExoInterface
    _interface  : ExoInterface
    _rointerface: ExoInterface
    _shadow     : ExoInterface
    _committed  : ExoInterface

    def __init__(self, func):
        self._lock= threading.Lock()

        sig= inspect.signature(func)
        interface= sig.parameters.get('interface').default
        if interface:
            interface= interface._clone()

        self._shadow= interface
        self.commit()
        self.refresh()

    def get_amode(self, ref):
        ifelem= self._shadow[ref] if ref else self._shadow
        return ifelem.amode()

    def extend(self, ref= '', amount= 1):
        ifelem= self._shadow[ref] if ref else self._shadow
        ifelem.extend(amount= amount)

    def attach(self, ref= '', accessor= None):
        port= self._shadow[ref] if ref else self._shadow
        port.accessor().attach(accessor= accessor)

    def detach(self, ref= ''):
        raise NotImplementedError

    def commit(self):
        with self._lock:
            self._committed= self._shadow
            self._shadow   = self._committed._shadow()

    def refresh(self):
        with self._lock:
            self._interface  = self._committed
            self._rointerface= rodict(self._interface)

    def __len__(self):
        return len(self._rointerface)

    def ports(self):
        return self._rointerface.ports()

    def __getattr__(self, key):
        v= getattr(self._rointerface, key)
        return v

    def __getitem__(self, key):
        return self._rointerface[key]


class EndoContext(EndoControl):
    _exocontext: object
    _executor  : object

    def __init__(self, exocontext):
        self._exocontext= exocontext
        self._executor  = None

    def _forward(self, function, **kwargs):
        raise NotImplementedError

    def forward(self, **kwargs):
        f= inspect.getouterframes(frame= inspect.currentframe(), context= 2)[1]
        try:
            self._forward(function= f.function, **kwargs)
        finally:
            del f

    def extend(self, ref= '', amount= 1):
        self.forward(ref= ref, amount= amount)

    def attach(self, ref= '', cd= None):
        self.forward(ref= ref, exocd= cd.exo_descriptor())

    def detach(self, ref= ''):
        self.forward(ref= ref)

    def commit(self):
        self.forward()



@dataclass
class ExoContext(EndoControl, ExoControl):
    _exoprocess : object = field(repr= False)
    _ctxif      : ContextInterface

    def __init__(self, exoprocess):
        self._exoprocess= exoprocess
        exoprocess._exo= self
        self._ctxif= ContextInterface(func= exoprocess.body)

    # interface manipulation

    def get_amode(self, ref=''):
        return self._ctxif.get_amode(ref= ref)

    def extend(self, ref= '', amount= 1):
        self._ctxif.extend(ref= ref, amount= amount)

    def attach(self, ref= '', exocd= None):
        #self._ctxif.attach(ref= ref, accessor= exocd.get_accessor(amode= self.get_amode(ref= ref)))
        self._ctxif.attach(ref= ref, accessor= exocd.get_accessor(amode= self.get_amode(ref= ref)))

    def detach(self, ref= ''):
        self._ctxif.detach(ref= ref)

    def commit(self):
        self._ctxif.commit()

    #>> process -> context

    def refresh(self):
        self._ctxif.refresh()

    def ports(self):
        return self._ctxif.ports()

    # +access to process interface (ports)

    @property
    def interface(self):
        return self._ctxif

    def __getattr__(self, key):
        v= self.interface[key]
        return v

    def __getitem__(self, key):
        return self.interface[key]
