import unittest
from ExoPort import *
from ExoPort import Access, Port, Source, Target


class ExoPortTest(unittest.TestCase):
    def _test_port(self, p):
        self.assertIs(None, p.value)
        self.assertIs(None, p.get())

        p.value = 1
        self.assertEqual(1, p.value)
        self.assertEqual(1, p.get())

        p.put(2)
        self.assertEqual(2, p.value)
        self.assertEqual(2, p.get())

        q= p._clone()
        p.value = 3
        self.assertEqual(2, q.value)

        q= p._shadow()
        p.value= 4
        self.assertEqual(4, q.value)

        self.assertEqual([p], p.ports())

    def test_Port(self):
        p= Port()
        self._test_port(p)

    def test_Source(self):
        s= Source()
        self._test_port(s)
        self.assertIs(Access.Source, s.amode())

    def test_Target(self):
        t= Target()
        self._test_port(t)
        self.assertIs(Access.Target, t.amode())

    def test_IFRecord(self):
        r= IFRecord(a=1,b=2,c=3)

        self.assertEqual(3, len(r))
        self.assertEqual(1, r.a)
        self.assertEqual(2, r.b)
        self.assertEqual(3, r.c)

        p= r.ports()
        self.assertEqual([1,2,3], p)

        c= r._clone()
        r.a= 7
        self.assertEqual(1, c.a)

    def test_IFVector(self):
        v= IFVector[[1]]

        self.assertEqual([1], v._vtype)
        self.assertEqual(0, len(v))

        v.extend()
        self.assertEqual(1, len(v))

        v.extend(amount= 2)
        self.assertEqual(3, len(v))

        v[0][0]= 2
        self.assertEqual(1, v[1][0])
        self.assertEqual(1, v[2][0])

    def test_Portal_empty(self):
        p= Portal()

        self.assertEqual(0, len(p))
        self.assertEqual([], p.ports())

    def test_Portal_altgroup(self):
        p= Portal(1, 2, 3)

        self.assertEqual(1, len(p))
        self.assertTrue('portal' in p)
        self.assertEqual([1,2,3], p.ports())

        self.assertEqual(1, p.portal[0])
        self.assertEqual(2, p.portal[1])
        self.assertEqual(3, p.portal[2])

    def test_Portal(self):
        v= IFVector[2]
        v.extend(amount= 3)
        p= Portal(a= IFRecord(1), b= v)

        self.assertEqual(2, len(p))
        self.assertEqual([1,2,2,2], p.ports())


if __name__ == '__main__':
    unittest.main()
