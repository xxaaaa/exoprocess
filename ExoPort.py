"""
Ports and interfaces.
"""

__all__ = ['ExoInterface', 'IFRecord', 'IFVector', 'S', 'T', 'Portal']

import copy

from common import *
import threading
import functools

from Channel.SMChannel import Access, Store
from hdict import *

logger= logging.getLogger(__name__)


@dataclass()
class ExoInterface:
    """hierarchical structure with ports as leaves"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _clone(self):
        """full duplicate"""
        raise NotImplementedError

    def _shadow(self):
        """duplicate, but ports are shared"""
        raise NotImplementedError

    def ports(self):
        """list of ports"""
        raise NotImplementedError


@dataclass
class Variable(Store):
    """one element Store"""
    _value: object

    def __init__(self):
        super().__init__(capacity= 1)
        self._value= None

    def size(self):
        return 1

    def nonempty(self):
        return True

    def nonfull(self):
        return True

    def get(self):
        return self._value

    def put(self, data):
        self._value= data

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value= value


class Port(ExoInterface, Variable):
    """base class for Source and Target"""

    def _clone(self):
        return copy.deepcopy(self)

    def _shadow(self):
        return self

    def __repr__(self):
        return stdrepr(self, 'v={}'.format(self.value))

    def ports(self):
        return [self]


class Source(Port):
    def amode(self):
        return Access.Source

    def recv(self):
        return threading.current_thread()._exoprocess.recv(port= self)


class Target(Port):
    def amode(self):
        return Access.Target

    def send(self, **kwargs):
        return threading.current_thread()._exoprocess.send(port= self, **kwargs)


class IFRecord(ExoInterface, hdict):
    """record (dict) of ExoInterfaces"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def _clone(self):
        return hdict._clone(self)

    def _shadow(self):
        return hdict._shadow(self)

    def __repr__(self):
        return stdrepr(self, '{}'.format(hdict._content_str(self)))

    def ports(self):
        return self._leaves()


class IFVector(IFRecord):
    """vector of ExoInterfaces"""

    def __init__(self):
        super().__init__()

    def __class_getitem__(cls, vtype):
        """create a vector with a given element type"""
        return type('{}[{}]'.format(cls.__name__, vtype), (cls,), dict(_vtype= vtype))()

    def extend(self, amount= 1):
        """extend the vector with amount number of elements (copies of vtype)"""

        self.append(*[copy.deepcopy(self._vtype) for i in range(amount)])


S= Source()  # placeholder for Source ports in interface declarations
T= Target()  # placeholder for Target ports in interface declarations


class Portal(xdict):
    """collection (dict) of ExoInterfaces (ports or port groups) that take part in a single interaction.
       keys identify alternatives, which are subgroups of ports.
       an alternative is ready if all its members are ready.
       the portal is ready if at least one alternative is ready.
    """

    def __init__(self, *altgroup, **alternatives):
        """create a portal from
           either an enumeration of a single group of alternatives (altgroup),
           or from a proper dict of alternatives.
        """

        assert not (bool(altgroup) and bool(alternatives))
        if bool(altgroup):
            alternatives= {'portal': IFRecord(*altgroup)}

        super().__init__(**alternatives)

    def ports(self):
        return functools.reduce(lambda x, y: x+y, [e.ports() for e in self.values()], [])

    #>> interaction calls are forwarded to the exoprocess

    def interaction(self):
        return threading.current_thread()._exoprocess.interaction(portal= self)

    def bcast(self, value):
        threading.current_thread()._exoprocess.bcast(targets= self, value= value)

