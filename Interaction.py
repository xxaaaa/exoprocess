__all__ = ['interaction']


from common import *
from Channel.Access import REvent


logger= logging.getLogger(__name__)


def channel_accessor(port):
    return port.accessor()._linked


def interaction(portal):
    def check_portal():
        """check for validity.
           allow the following(?)
           - empty portal (immediately ready)
           - empty alternative (immediately ready)
           - multiple alternatives to reference the same port (harmless)
        """
        pass

    def start_interaction():
        """prepare ports for interaction by setting the event on which they report readiness
        """
        for p in portal.ports():
            channel_accessor(port= p).setevent(ia_event)

    def close_interaction():
        """cancel readiness reporting.
           not strictly necessary: tradeoff between the cost of this and that of readiness reporting
        """
        for p in portal.ports():
            channel_accessor(port= p).setevent()

    def scan():
        """scan the ports and determine readiness
        :return: set of keys of ready alternatives
        """
        keys = []
        for altkey, altmember in portal.items():
            if all([channel_accessor(port= p).ready() for p in altmember.ports()]):
                keys.append(altkey)
                # if there is one ready alternative, then the portal is ready, so we could stop there...
                # but it is better to involve as many ports as possible
                # if keys:
                #     break
        return keys

    def do_interaction():
        """ loop until interaction is finished
        :return: set of keys of ready alternatives
        """
        # keys= scan()
        # while 0 == len(keys) and 0 < len(portal):
        #     ia_event.wait()
        #     keys= scan()

        keys= []
        while 0 < len(portal):
            keys= scan()
            if 0 < len(keys):
                break

        return keys

    def transfer(keys):
        """carry out transfer on those ports that are members of ready alternatives
        """
        for k in keys:
            for p in portal[k].ports():
                p.accessor().transfer()

    check_portal()
    ia_event= REvent()
    start_interaction()
    keys= do_interaction()
    close_interaction()

    transfer(keys= keys)
    return keys

