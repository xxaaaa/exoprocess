from common import *
from ExoProcess import *
import Context.DMContext


logging.basicConfig(level= logging.DEBUG, format= '[%(levelname)8s]:%(name)20s:%(process)5s--%(threadName)12s: %(message)s')
logger= logging.getLogger(scriptname())


class Hello(ExoProcess):
    def body(self, interface= S):
        logger.info('>> hello.')
        data= interface.recv()
        logger.info('<< hello {}'.format(data))


class World(ExoProcess):
    def body(self, interface= T):
        #logger.debug(self._exo)
        logger.info('>> world.')
        interface.send(value= 'WORLD')
        logger.info('<< world.')


class HelloWorld(ExoModule):
    def __init__(self):
        super().__init__(registrator= Context.DMContext.DMRegistrator)

    def body(self, interface= ExoModule.std_interface, **kwargs):
        hello= self.registerProcess(Hello())
        world= self.registerProcess(World())

        c= self.registerChannel(DMCD(name= 'c'))
        hello.attach(ref= '', cd= c)
        world.attach(ref= '', cd= c)

        logger.info('starting.')
        self.start_all(hello, world)
        self.join_all(hello, world)
        logger.info('finished.')


if __name__ == '__main__':
    HelloWorld().start()
