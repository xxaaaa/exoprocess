from common import *
from ExoProcess import *

logging.basicConfig(level= logging.DEBUG, format= '[%(levelname)8s]:%(name)16s:%(threadName)12s: %(message)s')
logger= logging.getLogger(scriptname())


class X(ExoProcess):
    def body(self, interface= IFRecord(a= S, b= T, c= IFVector[S], d= IFRecord(x= S, y= T))):
        print(interface)
        interface.b.value = 3
        print(interface)


class Ping(ExoProcess):
    def body(self, interface= IFRecord(out= T)):
        interface.out.value= 'hello'
        self.interaction(portal= Portal(out= interface.out))
        logger.debug('ping - {}'.format(interface.out.value))


class Pong(ExoProcess):
    def body(self, interface= IFRecord(inp= S)):
        self.interaction(portal= Portal(inp= interface.inp))
        logger.debug('pong - {}'.format(interface.inp.value))


class PingPong(ExoModule):
    def body(self, interface= ExoModule.std_interface):
        ci= self.registerProcess(Ping())
        co= self.registerProcess(Pong())
        q= self.registerChannel(SMCD(name= 'c'))
        ci.attach(ref= 'out', cd= q)
        co.attach(ref= 'inp', cd= q)

        co.start()
        logger.debug('-----------')
        logger.debug(q)

        ci.start()
        logger.debug('-----------')
        logger.debug(q)


if __name__ == '__main__':
    PingPong().start()

