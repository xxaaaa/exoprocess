import random

from common import *
from ExoProcess import *


logging.basicConfig(level= logging.DEBUG, format= '[%(levelname)8s]:%(name)16s:%(threadName)12s: %(message)s')
logger= logging.getLogger(scriptname())


EOS= -1


class Master(ExoProcess):
    def body(self, interface= IFRecord(out= IFVector[T])):
        for t in range(10):
            self.bcast(targets= interface.out, value= t)

        self.bcast(targets= interface.out, value= EOS)


class Slave(ExoProcess):
    def body(self, interface= IFRecord(inp= S)):
        going= True
        while going:
            t= interface.inp.recv()
            if EOS == t:
                going= False
            else:
                logger.debug('time: %s', t)


class Simulation(ExoModule):
    def body(self, size, interface= ExoModule.std_interface):
        m= self.registerProcess(Master())
        s= self.registerProcess(*[Slave(name= I['Slave', i]) for i in range(size)])

        c = self.registerChannel(*[SMCD(name= I['c', i]) for i in range(size)])
        m.extend(ref= 'out', amount= size)

        for i in range(size):
            m.attach(ref= 'out.{}'.format(i), cd= c[i])
            s[i].attach(ref= 'inp', cd= c[i])

        self.start_all(m, *s)


if __name__ == '__main__':
    Simulation().start(size= 5)