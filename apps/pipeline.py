import random

from common import *
from ExoProcess import *
import Context.DMContext


logging.basicConfig(level= logging.DEBUG, format= '[%(levelname)8s]:%(name)20s:%(process)5s--%(threadName)12s: %(message)s')
logger= logging.getLogger(scriptname())
#logging.basicConfig(level= logging.DEBUG, format= '[%(levelname)8s]:%(name)16s:%(threadName)12s: %(message)s')


class PipeElementA(ExoProcess):
    def body(self, size, interface= IFRecord(inp= S, out= T)):
        #inp= interface.inp
        #out= interface.out

        largest= self.inp.recv()

        for i in range(1, size):
            current= self.inp.recv()

            oc, ol = current, largest
            current, largest = min(current, largest), max(current, largest)
            logger.info('%s <? %s - %s > %s', oc, ol, largest, current)

            self.out.send(value= current)

        self.out.send(value= largest)


class PipeElementX(ExoProcess):
    def body(self, size, interface= IFRecord(inp= S, out= T)):
        # inp= interface.inp
        # out= interface.out

        largest= self.inp.recv()
        self.inp.recv()

        for i in range(2, size):
            olarge= largest
            self.out.value, largest = min(self.inp.value, largest), max(self.inp.value, largest)
            logger.info('%s <? %s - %s > %s', self.inp.value, olarge, largest, self.out.value)

            Portal(self.inp, self.out).interaction()

        self.out.value, largest = min(self.inp.value, largest), max(self.inp.value, largest)
        self.out.send()
        self.out.send(value= largest)


class PipeElementR(ExoReactor):
    def terminate(self):
        return 0 == self.size

    def prologue(self):
        self.largest= self.inp.recv()
        self.inp.recv()
        self.size -= 1

    def epilogue(self):
        self.out.send()
        self.out.send(value= self.largest)

    def compute(self):
        olarge= self.largest
        self.out.value, self.largest = sorted([self.inp.value, self.largest])
        logger.info('%s <? %s - %s > %s', self.inp.value, olarge, self.largest, self.out.value)
        self.size -= 1

    def communicate(self):
        Portal(*self.ports()).interaction()

    def body(self, size, interface= IFRecord(inp= S, out= T)):
        self.size= size
        super().body()


class PipeHead(ExoProcess):
    def body(self, size, interface= IFRecord(inp= S, out= T)):
        # inp= interface.inp
        # out= interface.out

        v= list(range(size))
        random.shuffle(v)
        logger.info(' input: %s', v)

        for i in v:
            #out.value= i
            #self.interaction(portal= Portal(out= out))
            self.out.send(value= i)

        for i in range(len(v)):
            #self.interaction(portal= Portal(inp= inp))
            #v[i]= inp.value
            v[i]= self.inp.recv()

        logger.info('output: %s', v)


class Pipeline(ExoModule):
    def __init__(self):
        super().__init__(registrator= Context.DMContext.DMRegistrator)

    def body(self, eClass, interface= ExoModule.std_interface):
        size= 5
        name = eClass.__name__[-1]

        elem= self.registerProcess(*[eClass(name= I[name, i]) for i in range(size)])
        head= self.registerProcess(PipeHead(name= name))

        pipe= self.registerChannel(*[DMCD(name= I['C', i], capacity= 1) for i in range(size + 1)])

        head.attach(ref= 'out', cd= pipe[0])
        for i in range(size):
            elem[i].attach(ref= 'inp', cd= pipe[i])
            elem[i].attach(ref= 'out', cd= pipe[i + 1])
        head.attach(ref= 'inp', cd= pipe[size])

        self.start_all(head, *elem, size= size)
        self.join_all(head, *elem)


if __name__ == '__main__':
    Pipeline().start(eClass= PipeElementA)
    logger.info('-'*80)
    Pipeline().start(eClass= PipeElementX)
    logger.info('-'*80)
    Pipeline().start(eClass= PipeElementR)

