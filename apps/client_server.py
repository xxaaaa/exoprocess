import random

from common import *
from ExoProcess import *


logging.basicConfig(level= logging.DEBUG, format= '[%(levelname)8s]:%(name)16s:%(threadName)12s: %(message)s')
logger= logging.getLogger(scriptname())


REQ_TERM= -1


class Server(ExoProcess):
    def body(self, interface= IFVector[IFRecord(req= S, ans= T)]):
        size= len(interface)
        while 0 < size:
            #p= Portal(**{k:i.req for k, i in interface.items()})
            #p= Portal(**interface[...].req)
            p= Portal(**self[...].req)
            logger.debug('portal: {}'.format(p))

            ia= p.interaction()
            #logger.debug('$$$ request received {}'.format(ia))

            for r in ia:
                request= self[r].req.value
                if REQ_TERM == request:
                    size -= 1
                else:
                    self[r].ans.send(value= request + '-X')


class Client(ExoProcess):
    def body(self, interface= IFRecord(req= T, ans= S)):
        size= random.randint(1, 10)
        for i in range(size):
            self.req.send(value= 'REQUEST {}-{:03}'.format(self._name, i))
            logger.debug('request sent "{}"'.format(self.req.value))

            answer= self.ans.recv()
            logger.debug('answer received {}'.format(answer))

        self.req.send(value= REQ_TERM)


class ClientServer(ExoModule):
    def body(self, size, interface= ExoModule.std_interface):
        server  = self.registerProcess(Server())
        clients = self.registerProcess(*[Client(name= I['Client', i]) for i in range(size)])

        reqchannels= self.registerChannel(*[SMCD(name= I['Req', i]) for i in range(size)])
        anschannels= self.registerChannel(*[SMCD(name= I['Ans', i]) for i in range(size)])
        channels = [c for c in zip(reqchannels, anschannels)]

        server.extend(amount= size)
        for i in range(size):
            server.attach(ref= '{}.req'.format(i), cd= channels[i][0])
            clients[i].attach(ref= 'req', cd= channels[i][0])

            server.attach(ref= '{}.ans'.format(i), cd= channels[i][1])
            clients[i].attach(ref= 'ans', cd= channels[i][1])

        self.start_all(server, *clients)


if __name__ == '__main__':
    ClientServer().start(size= 3)



