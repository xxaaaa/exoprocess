from common import *
from ExoProcess import *

import time
import random


logging.basicConfig(level= logging.DEBUG, format= '[%(levelname)8s]:%(name)16s:%(threadName)18s: %(message)s')
logger= logging.getLogger(scriptname())


class Publisher(ExoProcess):
    def body(self, interface= IFVector[T]):
        data= 0
        while data < 20:
            data += 1
            time.sleep(random.randint(1,3))

            self.refresh()
            logger.debug('publishing {:03} to {:03} subscribers'.format(data, len(interface)))
            Portal(*interface.ports()).bcast(value= data)


class Subscriber(ExoProcess):
    def body(self, interface= S):
        count= random.randint(3, 10)
        logger.debug('consuming {:03} messages'.format(count))

        for i in range(count):
            data= interface.recv()
            logger.debug('received {:03}'.format(data))

        logger.debug('consumed {:03} messages'.format(count))


class PubSub(ExoModule):
    def subscribe(self, pub, i):
        c= self.registerChannel(SMCD(name= I['sub', i]))
        pub.extend()
        pub.attach(ref= '-1', cd= c)

        sub= self.registerProcess(Subscriber(name= I['Sub', i]))

        sub.attach(cd= c)
        pub.commit()
        sub.start()

    def body(self, interface= ExoModule.std_interface):
        pub= self.registerProcess(Publisher())
        pub.start()

        self.subscribe(pub, 1)
        time.sleep(3)
        self.subscribe(pub, 2)
        time.sleep(3)
        self.subscribe(pub, 3)


if __name__ == '__main__':
    PubSub().start()



