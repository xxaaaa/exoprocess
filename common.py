"""common imports and definitions
"""

from dataclasses import dataclass, field
import logging
import sys


def scriptname():
    prefix, _, name = sys.argv[0].rpartition('/')
    return name


def stdrepr(obj, details):
    return '{}({})'.format(obj.__class__.__name__, details)


class I:
    def __class_getitem__(cls, item):
        return '{}[{:03}]'.format(item[0], item[1])


# def adjustname(name):
#     pre, _, post = name.partition('[')
#     if post:
#         post= int(post[:-1])
#         post= '[{:03}]'.format(post)
#
#     return pre+post


